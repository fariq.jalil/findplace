import React from "react";

import Container from "components/layout/Container";
import PlaceAutocomplete from "components/PlaceAutocomplete";

const FindPlace = () => {
  return (
    <Container>
      <PlaceAutocomplete />
    </Container>
  );
};

export default FindPlace;
