import { takeLatest, all } from "redux-saga/effects";

import { updatePlaces } from "./generators/places";
import * as placesActionTypes from "./actionTypes/places";

function* rootSaga() {
  yield all([yield takeLatest(placesActionTypes.GET_PLACES, updatePlaces)]);
}

export default rootSaga;
