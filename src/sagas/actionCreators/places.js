import * as actionTypes from "sagas/actionTypes/places";

export const updatePlaces = (places = []) => ({
  type: actionTypes.GET_PLACES,
  payload: {
    places
  }
});
