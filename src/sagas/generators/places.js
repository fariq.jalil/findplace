import { put } from "redux-saga/effects";

import * as actionTypes from "sagas/actionTypes/places";

export function* updatePlaces(action) {
  const places = action.payload.places.map((result) => ({
    description: result.description,
    placeId: result.place_id
  }));
  yield put({
    type: actionTypes.UPDATE_PLACES,
    payload: {
      places
    }
  });
}
