const loadScript = (src, id) => {
  const script = document.createElement("script");
  script.setAttribute("async", true);
  script.setAttribute("defer", true);
  script.setAttribute("id", id);
  script.src = src;
  document.body.appendChild(script);
};

export default loadScript;
