import * as actionTypes from "sagas/actionTypes/places";

const placesReducer = (state = [], action) => {
  switch (action.type) {
    case actionTypes.GET_PLACES:
      return action.payload.places;
    case actionTypes.UPDATE_PLACES:
      return action.payload.places;
    default:
      return state;
  }
};

export default placesReducer;
