import { combineReducers } from "redux";

import places from "reducers/places";

export default combineReducers({ places });
