import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { TextField as TextFieldMUI } from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    width: "100%"
  }
});

const TextField = ({ name, label, ...rest }) => {
  const classes = useStyles();

  return <TextFieldMUI name={name} className={classes.root} label="Enter place name" {...rest} />;
};

export default TextField;
