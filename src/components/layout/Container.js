import React from "react";
import { Container as ContainerMUI, Grid, Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import NavBar from "components/layout/NavBar";

const useStyles = makeStyles({
  root: {
    marginTop: "100px"
  },
  paperRoot: {
    padding: "30px",
    opacity: 0.97
  }
});

const Container = ({ children }) => {
  const classes = useStyles();

  return (
    <ContainerMUI maxWidth={false} disableGutters>
      <NavBar />
      <Grid container className={classes.root} justify="center">
        <Grid item xs={12} sm={10}>
          <Paper className={classes.paperRoot} elevation={5}>
            <ContainerMUI maxWidth="lg">{children}</ContainerMUI>
          </Paper>
        </Grid>
      </Grid>
    </ContainerMUI>
  );
};

export default Container;
