import React from "react";
import { AppBar, IconButton, Toolbar, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import AccountCircle from "@material-ui/icons/AccountCircle";

const useStyles = makeStyles({
  title: {
    flexGrow: 1
  }
});

const NavBar = () => {
  const classes = useStyles();

  return (
    <AppBar color="default">
      <Toolbar>
        <Typography variant="h6" className={classes.title}>
          Find Place
        </Typography>
        <IconButton
          onClick={() => window.open("https://gitlab.com/fariq.jalil")}
          color="inherit"
          title="GitLab"
        >
          <AccountCircle />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
};

export default NavBar;
