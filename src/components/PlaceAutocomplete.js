import React, { useEffect, useState } from "react";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { useSelector, useDispatch } from "react-redux";

import useMaps from "hooks/useMaps";
import { updatePlaces } from "sagas/actionCreators/places";
import { TextField } from "components/fields";
import Map from "components/Map";

let autoCompleteService = null;
let geocoder = null;

const PlaceAutocomplete = () => {
  const dispatch = useDispatch();
  const mapRef = React.useRef(false);
  const { places } = useSelector(state => state);
  const [coordinate, setCoordinate] = useState({
    lat: 3.139,
    lng: 101.6869
  }); // default KL
  const [placeId, setPlaceId] = useState("");
  const maps = useMaps({ libraries: "places", ref: mapRef });

  if (!autoCompleteService && maps) {
    autoCompleteService = new maps.places.AutocompleteService();
  }
  if (!geocoder && maps) {
    geocoder = new maps.Geocoder();
  }

  useEffect(() => {
    if (maps) {
      const map = new maps.Map(document.getElementById("map"), {
        center: {
          lat: coordinate.lat,
          lng: coordinate.lng
        },
        zoom: 15
      });

      const marker = new maps.Marker({ position: coordinate });
      marker.setMap(map);
    }
  });

  useEffect(() => {
    if (maps) {
      geocoder.geocode({ placeId }, function(results, status) {
        if (status === "OK") {
          if (results[0]) {
            setCoordinate({
              lat: results[0].geometry.location.lat(),
              lng: results[0].geometry.location.lng()
            });
          }
        }
      });
    }
  }, [placeId]);

  const onInputChange = (_, value) => {
    if (value) {
      autoCompleteService.getPlacePredictions({ input: value }, (results, status) => {
        if (status === "OK") {
          dispatch(updatePlaces(results));
        }
      });
    }
  };

  const onSelectOption = (option, value) => {
    if (value.placeId === option.placeId) {
      setPlaceId(option.placeId);
      return true;
    }
  };

  return (
    <>
      <Autocomplete
        id="autocomplete-input"
        options={places}
        getOptionLabel={option => option.description}
        getOptionSelected={onSelectOption}
        onInputChange={onInputChange}
        blurOnSelect
        renderInput={params => (
          <TextField name="autocomplete-input" label="Enter place name" {...params} />
        )}
      />
      <br />
      <br />
      <Map id="map" width="100%" height="400px" />
    </>
  );
};

export default PlaceAutocomplete;
