import React from "react";
import PropTypes from "prop-types";

const Map = ({ id, height, width }) => {
  return <div id={id} style={{ height, width }} />;
};

Map.propTypes = {
  id: PropTypes.string.isRequired,
  height: PropTypes.string.isRequired,
  width: PropTypes.string.isRequired
};

export default Map;
