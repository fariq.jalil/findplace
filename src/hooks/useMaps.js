import { useState, useEffect } from "react";

import loadScript from "utils/loadScript";

const useMaps = ({ libraries, ref }) => {
  const [maps, setMaps] = useState(null);

  useEffect(() => {
    if (typeof window !== "undefined" && !ref.current) {
      if (!document.querySelector("#google-maps")) {
        loadScript(
          `https://maps.googleapis.com/maps/api/js?key=${process.env.REACT_APP_GOOGLE_API}&libraries=${libraries}`,
          "google-maps"
        );
      }

      ref.current = true;
    }

    window.onload = x => {
      setMaps(window.google.maps);
    };
  });

  return maps;
};

export default useMaps;
