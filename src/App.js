import React, { lazy, Suspense } from "react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import { CircularProgress } from "@material-ui/core";

import rootReducer from "reducers";
import rootSaga from "sagas";

const FindPlace = lazy(() => import("pages/FindPlace"));

const sagaMiddleware = createSagaMiddleware();
const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);

const App = () => {
  return (
    <Provider store={store}>
      <Suspense fallback={<CircularProgress />}>
        <FindPlace />
      </Suspense>
    </Provider>
  );
};

export default App;
