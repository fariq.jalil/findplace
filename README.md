## About

The purpose of this project is to showcase the use of:

- React Hooks.
- Redux.
- Redux-Saga.
- Google Maps places API.
- Material-UI.

This project require internet connection to run in order to connect with Google Maps Api.

**Note: Replace "INSERT_GOOGLE_API_KEY" in .env file with valid key.**

> .env<br/>REACT_APP_GOOGLE_API="INSERT_GOOGLE_API_KEY"

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**
